package com.example.transportapp;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.transportapp.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.SupportMapFragment;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.RoutingError;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.Nullable;

public class seepz extends AppCompatActivity {
/*

    public static final String Latitude = "LATITUDE",
            Longitude = "LONGITUDE",
            STARTLAT = "START POINT LAT",
            STARTLONG = "START POINT LONG";

    private SupportMapFragment mapFragment = null;
    Map map = null;

    public Double Lati;
    public Double Longi;
    public Double Slat;
    public Double Slong;

    com.here.android.mpa.common.Image Loc;
    com.here.android.mpa.common.Image Start;
    com.here.android.mpa.common.Image End;


    Spinner seepz;

    String Loc1;

    String location_IDO1[] = {"Select Bus", "Borivali-East(Fast WEH)", "Borivali-West(SV Road)", "Byculla (Moms Kitchan) via Ghatkoper-West", "Thane Kopri",
            "Vikhroli-West", "Andheri Mahada", "Andheri Railway Station", "Marine Lines_Dadar-West", "Bhayander Railway Station-West"};

    ArrayAdapter<String> arrayAdapter1;

    //DocumentReference Docref = FirebaseFirestore.getInstance().collection("GPS_Data").document("Location");


    MapMarker mapmaker;

*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seepz);

/*
        seepz = findViewById(R.id.spinner_seepz);

        arrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, location_IDO1);

        seepz.setAdapter(arrayAdapter1);

        seepz.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(com.example.transportapp.seepz.this,"You Have Selected: "+location_IDO1[position],Toast.LENGTH_SHORT).show();

                Loc1 = location_IDO1[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        DocumentReference Docref = FirebaseFirestore.getInstance().collection("GPS_Data").document("Nodal Buses").collection("Seepz").document("Vikhroli-West");

        Docref.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {

                setLocationOnMap(documentSnapshot);
            }
        });


        Docref.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {

                    Lati = documentSnapshot.getDouble(Latitude);
                    Longi = documentSnapshot.getDouble(Longitude);
                    Slat = documentSnapshot.getDouble(STARTLAT);
                    Slong = documentSnapshot.getDouble(STARTLONG);
                    mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
                    boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(getApplicationContext()
                            .getExternalFilesDir(null) + File.separator + ".here-maps", "com.here.android.mpa.service.MapService.v3");


                    mapFragment.init(new OnEngineInitListener() {
                        @Override
                        public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                            if (error == OnEngineInitListener.Error.NONE) {

// now the map is ready to be used
                                map = mapFragment.getMap();
                                map.setZoomLevel(10);
                                map.setCenter(new GeoCoordinate(19.125720, 72.874822), Map.Animation.NONE);

//Sets icon on Marker
                                Loc = new com.here.android.mpa.common.Image();
                                Start = new com.here.android.mpa.common.Image();
                                End = new com.here.android.mpa.common.Image();
                                try {
                                    Loc.setImageResource(R.drawable.bus);
                                    Start.setImageResource(R.drawable.start);
                                    End.setImageResource(R.drawable.end);
                                } catch (IOException e) {
                                    finish();
                                }

//Creates Marker Points

                                    mapmaker = new MapMarker(new GeoCoordinate(Lati, Longi), Loc);

                                MapMarker myMapMarker1 =
                                        new MapMarker(new GeoCoordinate(Slat, Slong), Start);

                                MapMarker myMapMarker =
                                        new MapMarker(new GeoCoordinate(19.125574, 72.874605), End);

                                map.addMapObject(mapmaker);
                                map.addMapObject(myMapMarker);
                                map.addMapObject(myMapMarker1);
// Create a gesture listener and add it to the SupportMapFragment


                                MapGesture.OnGestureListener listener =
                                        new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
                                            @Override
                                            public boolean onMapObjectsSelected(List<ViewObject> objects) {
                                                for (ViewObject viewObj : objects) {
                                                    if (viewObj.getBaseType() == ViewObject.Type.USER_OBJECT) {
                                                        if (((MapObject) viewObj).getType() == MapObject.Type.MARKER) {
                                                            // At this point we have the originally added
                                                            // map marker, so we can do something with it
                                                            // (like change the visibility, or more
                                                            // marker-specific actions)
                                                            ((MapObject) viewObj).setVisible(false);
                                                        }
                                                    }
                                                }
                                                // return false to allow the map to handle this callback also
                                                return false;
                                            }

                                        };

                                CoreRouter router = new CoreRouter();

// Create the RoutePlan and add two waypoints

                                RoutePlan routePlan = new RoutePlan();
                                routePlan.addWaypoint(new RouteWaypoint(new GeoCoordinate(Slat, Slong)));
                                routePlan.addWaypoint(new RouteWaypoint(new GeoCoordinate(19.125574, 72.874605)));

                                // Create the RouteOptions and set its transport mode & routing type
                                RouteOptions routeOptions = new RouteOptions();
                                routeOptions.setTransportMode(RouteOptions.TransportMode.PEDESTRIAN);
                                routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
                                routeOptions.setRouteType(RouteOptions.Type.FASTEST);

                                routePlan.setRouteOptions(routeOptions);

// Calculate the route

                                router.calculateRoute(routePlan, new seepz.RouteListener());

                            } else {

                                System.out.println("ERROR: Cannot initialize SupportMapFragment--"+error.getDetails());

                            }


                        }
                    });


                }
            }
        });
*/


    }
/*
    private class RouteListener implements CoreRouter.Listener {

// Method defined in Listener

        public void onProgress(int percentage) {

// Display a message indicating calculation progress

        }

// Method defined in Listener

        public void onCalculateRouteFinished(List<RouteResult> routeResult, RoutingError error) {

// If the route was calculated successfully

            if (error == RoutingError.NONE) {

// Render the route on the map

                MapRoute mapRoute = new MapRoute(routeResult.get(0).getRoute());

                map.addMapObject(mapRoute);

            } else {

// Display a message indicating route calculation failure

                Toast.makeText(com.example.transportapp.seepz.this, "Not able to load route", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setLocationOnMap(DocumentSnapshot documentSnapshot) {


        if (mapmaker != null) {
            Lati = documentSnapshot.getDouble(Latitude);
            Longi = documentSnapshot.getDouble(Longitude);
       *//*     mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
            map = mapFragment.getMap();
            map.removeMapObject(mapmaker);
            mapmaker = new MapMarker(new GeoCoordinate(Lati, Longi));
            map.addMapObject(mapmaker);
*//*
            GeoCoordinate cordinates = mapmaker.getCoordinate();
            cordinates.setLatitude(Lati);
            cordinates.setLongitude(Longi);
            mapmaker.setCoordinate(cordinates);


        }


    }*/







}
