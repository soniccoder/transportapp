package com.example.transportapp;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.mapping.MapMarker;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.transportapp.gigaplex.Bus_num;
import static com.example.transportapp.gigaplex.posi;

public class ElixiaClient {


    private Handler mHandler;

    //public Double Latitude, Longitude;

    static com.here.android.mpa.mapping.Map map = null;
    public static MapMarker currentMapMark;
    static String busNumber;

    public ElixiaClient(com.here.android.mpa.mapping.Map map, MapMarker currentMapMark, String busNumber) {
        this.map = map;
        //this.currentMapMark = currentMapMark;
        this.busNumber = busNumber;
    }

    public void load() {

        this.mHandler = new Handler();
        this.mHandler.postDelayed(m_Runnable, 5000);

    }

    private final Runnable m_Runnable = new Runnable() {
        String localBus;
        public void run()
        {
            //Toast.makeText(getApplicationContext(), "in runnable", Toast.LENGTH_SHORT).show();

            if(localBus == null)
                localBus = busNumber;

            if(busNumber != null && localBus != null && busNumber.equals(localBus)) {
                mHandler.postDelayed(m_Runnable, 5000);

                System.out.println("This is ElixiaClient Dropbox selection............." + posi);


                //DocumentReference Docref = FirebaseFirestore.getInstance().collection("GPS_Data").document("Nodal Buses").collection("Gigaplex").document(posi);


                AsyncTask asyncTask = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] objects) {

                        OkHttpClient client = new OkHttpClient();
                        System.out.println("Bus Number: " + busNumber);
                        Request request = new Request.Builder()
                                //.url("http://speed.elixiatech.com/modules/api/vts/api.php?action=getvehicledata&jsonreq={%22userkey%22:%22d6054de67e81908011b7edf7e0b514299bdd9533%22,%22searchstring%22:%22MH%2004%20FK%203070%22,%22pageindex%22:%221%22,%22pagesize%22:%222%22}")
                                .url("http://speed.elixiatech.com/modules/api/vts/api.php?action=getvehicledata&jsonreq={%22userkey%22:%22d6054de67e81908011b7edf7e0b514299bdd9533%22,%22searchstring%22:%22" + busNumber + "%22,%22pageindex%22:%221%22,%22pagesize%22:%222%22}")
                                .build();

                        Response response = null;

                        try {

                            response = client.newCall(request).execute();
                            return response.body().string();


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        return null;
                    }


                    @Override
                    protected void onPostExecute(Object o) {
                        try {

                            JSONObject jo = new JSONObject(o.toString());

                            String beta = "STATUS  :" + jo.get("Status");
                            String a = "MESSAGE  :" + jo.get("Message");
                            String ab = "RESULT  :" + jo.get("Result");

                            System.out.println("........................" + beta + " " + a + " " + ab);
                            //                            JSONArray jsonArray = new JSONArray(alpha);


                            JSONArray jsonArray = (JSONArray) jo.get("Result");

                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject jsonObject = (JSONObject) jsonArray.get(j);
                                Double Latitude = 0d, Longitude = 0d;
                                Bus_num = "VEHICAL NUMBER: " + jsonObject.get("vehicleno");
                                try {
                                    Latitude = Double.parseDouble(jsonObject.get("lat").toString());
                                    System.out.println(Latitude);
                                    Longitude = Double.parseDouble(jsonObject.get("lng").toString());
                                    System.out.println(Longitude);
                                } catch (NumberFormatException nfe) {

                                }

                                if (map != null) {
                                    if (currentMapMark != null)
                                        map.removeMapObject(currentMapMark);

                                    if (Latitude > 0d && Longitude > 0d) {
                                        currentMapMark = new MapMarker(new GeoCoordinate(Latitude, Longitude));
                                        map.addMapObject(currentMapMark);
                                    }
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        gigaplex.textView.setText(Bus_num);
                    }
                }.execute();
            }
        }

    };


}
