package com.example.transportapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class loadpage extends AppCompatActivity {

        private int timer=2;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_loadpage);

            LogoLauncher logoLauncher=new LogoLauncher();
            logoLauncher.start();
        }
        private class LogoLauncher extends Thread{

            public void run(){

                try{
                    sleep(1000*timer);
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
                Intent intent= new Intent(loadpage.this,MainActivity.class);
                startActivity(intent);
               loadpage.this.finish();
            }
        }
    }