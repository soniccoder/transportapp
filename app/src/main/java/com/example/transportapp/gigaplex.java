package com.example.transportapp;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.SupportMapFragment;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.RoutingError;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class gigaplex extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private SupportMapFragment mapFragment = null;
    Map map = null;

    public Double Slat, Slong, S1_lat, S1_long, S2_lat, S2_long, S3_lat, S3_long;

    com.here.android.mpa.common.Image Loc;
    com.here.android.mpa.common.Image Start;
    com.here.android.mpa.common.Image End;

    Spinner seepz;

    public static String posi;

    String location_IDO1[] = {"Thakurli","Kalamboli","Thane GB", "Ulhasnagar 1", "Ulhasnagar 2", "Kalamboli"};

    ArrayAdapter<String> arrayAdapter1;

    MapMarker currentMapMarker, startMapMarker, endMapMarker, stopMapMarker1, stopMapMarker2, stopMapMarker3;

    CoreRouter router;

    MapRoute mapRoute;

    public static TextView textView;

    //    String dataparsed = "";
    //    public String busnum = "MH 06 S 9120";

    public static String Bus_num;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gigaplex);

        seepz = findViewById(R.id.spinner_seepz);

        textView = findViewById(R.id.Bus_Number);

        arrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, location_IDO1);

        seepz.setAdapter(arrayAdapter1);

        seepz.setOnItemSelectedListener(this);




    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        posi = parent.getItemAtPosition(position).toString();

        Toast.makeText(parent.getContext(), posi, Toast.LENGTH_SHORT).show();

        System.out.println("this is the selected dropdown............................................................................" + posi);

        DocumentReference Docref = FirebaseFirestore.getInstance().collection("GPS_Data").document("Nodal Buses").collection("Gigaplex").document(posi);

        /*Docref.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {

                setLocationOnMap(documentSnapshot);

            }

        });*/

        Docref.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {

                    Bus_num = documentSnapshot.getString("BUS_NUMBER");
                    Slat =Double.parseDouble(documentSnapshot.getString("START_POINT_LAT"));
                    Slong = Double.parseDouble(documentSnapshot.getString("START_POINT_LONG"));
                    //Latitude = documentSnapshot.getDouble(LATI);
                    //Longitude = documentSnapshot.getDouble(LONGI);


                    System.out.println("Start Posi: " + Slat + " //// " + Slong);


                    mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
                    boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(getApplicationContext()
                            .getExternalFilesDir(null) + File.separator + ".here-maps", "com.here.android.mpa.service.MapService.v3");

                    mapFragment.init(error -> {
                        if (error == OnEngineInitListener.Error.NONE) {

// now the map is ready to be used


                            map = mapFragment.getMap();
                            map.setZoomLevel(10);
                            map.setCenter(new GeoCoordinate(19.125720, 72.874822), Map.Animation.NONE);

//Sets icon on Marker
                            Loc = new com.here.android.mpa.common.Image();
                            Start = new com.here.android.mpa.common.Image();
                            End = new com.here.android.mpa.common.Image();
                            try {
                                Loc.setImageResource(R.drawable.bus);
                                Start.setImageResource(R.drawable.start);
                                End.setImageResource(R.drawable.end);
                            } catch (IOException e) {
                                finish();
                            }

//Creates Marker Points

                            if (ElixiaClient.currentMapMark != null)
                                map.removeMapObject(ElixiaClient.currentMapMark);
                            if(startMapMarker !=null)
                                map.removeMapObject(startMapMarker);
                            //      map.removeMapObject(stopMapMarker1);
                            //      map.removeMapObject(stopMapMarker2);
                            //      map.removeMapObject(stopMapMarker3);
                            if(endMapMarker != null)
                                map.removeMapObject(endMapMarker);


                            //currentMapMarker = new MapMarker(new GeoCoordinate(Latitude, Longitude), Loc);

                            startMapMarker = new MapMarker(new GeoCoordinate(Slat, Slong), Start);

                            /*

                            stopMapMarker1 = new MapMarker(new GeoCoordinate(S1_lat, S1_long));

                            stopMapMarker2 = new MapMarker(new GeoCoordinate(S2_lat, S2_long));

                            stopMapMarker3 = new MapMarker(new GeoCoordinate(S3_lat, S3_long));

                            */

                            endMapMarker = new MapMarker(new GeoCoordinate(19.174685, 72.992235), End);

                            //map.addMapObject(currentMapMarker);
                            map.addMapObject(endMapMarker);
                            map.addMapObject(startMapMarker);

                          /*

                            map.addMapObject(stopMapMarker1);
                            map.addMapObject(stopMapMarker2);
                            map.addMapObject(stopMapMarker3);

                          */

// Create a gesture listener and add it to the SupportMapFragment


                            MapGesture.OnGestureListener listener =
                                    new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
                                        @Override
                                        public boolean onMapObjectsSelected(List<ViewObject> objects) {
                                            for (ViewObject viewObj : objects) {
                                                if (viewObj.getBaseType() == ViewObject.Type.USER_OBJECT) {
                                                    if (((MapObject) viewObj).getType() == MapObject.Type.MARKER) {
                                                        // At this point we have the originally added
                                                        // map marker, so we can do something with it
                                                        // (like change the visibility, or more
                                                        // marker-specific actions)
                                                        ((MapObject) viewObj).setVisible(false);
                                                    }
                                                }
                                            }
                                            // return false to allow the map to handle this callback also
                                            return false;
                                        }

                                    };


                            router = new CoreRouter();

// Create the RoutePlan and add two waypoints

                            RoutePlan routePlan = new RoutePlan();

                            if (routePlan != null) {
                                routePlan.removeAllWaypoints();

                            }
                            routePlan.addWaypoint(new RouteWaypoint(new GeoCoordinate(Slat, Slong)));
                            routePlan.addWaypoint(new RouteWaypoint(new GeoCoordinate(19.174685, 72.992235)));


                            // Create the RouteOptions and set its transport mode & routing type
                            RouteOptions routeOptions = new RouteOptions();
                            //routeOptions.setTransportMode(RouteOptions.TransportMode.PEDESTRIAN);
                            routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
                            //routeOptions.setRouteType(RouteOptions.Type.FASTEST);

                            routePlan.setRouteOptions(routeOptions);

// Calculate the route

                            router.calculateRoute(routePlan, new gigaplex.RouteListener());

                        } else {

                            System.out.println("ERROR: Cannot initialize SupportMapFragment--" + error.getDetails());

                        }


                    });


                }
            }
        });


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }

    private class RouteListener implements CoreRouter.Listener {

// Method defined in Listener

        public void onProgress(int percentage) {

// Display a message indicating calculation progress

        }

// Method defined in Listener

        public void onCalculateRouteFinished(List<RouteResult> routeResult, RoutingError error) {

// If the route was calculated successfully

            if (mapRoute != null) {
                map.removeMapObject(mapRoute);
            }

            if (error == RoutingError.NONE) {

// Render the route on the map


                mapRoute = new MapRoute(routeResult.get(0).getRoute());

                map.addMapObject(mapRoute);
                ElixiaClient cl = new ElixiaClient(map, currentMapMarker, Bus_num);
                cl.load();


            } else {

// Display a message indicating route calculation failure

                Toast.makeText(com.example.transportapp.gigaplex.this, "Not able to load route", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setLocationOnMap(DocumentSnapshot documentSnapshot) {

        if (currentMapMarker != null) {
//            Lati = documentSnapshot.getDouble(Latitude);
//            Longi = documentSnapshot.getDouble(Longitude);

            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
            map = mapFragment.getMap();
            map.removeMapObject(currentMapMarker);
            //currentMapMarker = new MapMarker(new GeoCoordinate(Latitude, Longitude));
            map.addMapObject(currentMapMarker);


            // GeoCoordinate cordinates = currentMapMarker.getCoordinate();
            // cordinates.setLatitude(Lati);
            // cordinates.setLongitude(Longi);
            // currentMapMarker.setCoordinate(cordinates);

        }


    }
}




