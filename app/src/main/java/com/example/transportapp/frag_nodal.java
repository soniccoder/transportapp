package com.example.transportapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class frag_nodal extends Fragment {


    Button ido1,ido2,ido3,ido4,ido5;


    public frag_nodal() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_frag_nodal, container, false);
        ido1 = view.findViewById(R.id.ido_btn1);
        ido1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), seepz.class);
                startActivity(intent);
            }
        });

        ido2 = view.findViewById(R.id.ido_btn2);
        ido2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), gigaplex.class);
                startActivity(intent);
            }
        });

       /* ido3 = view.findViewById(R.id.ido_btn3);
        ido3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), nodal_IDO_3.class);
                startActivity(intent);
            }
        });

        ido4 = view.findViewById(R.id.ido_btn4);
        ido4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), nodal_IDO_4.class);
                startActivity(intent);
            }
        });

        ido5 = view.findViewById(R.id.ido_btn5);
        ido5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), nodal_IDO_5.class);
                startActivity(intent);
            }
        });

*/
        return view;
    }

}
