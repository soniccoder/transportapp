package com.example.transportapp;
import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.getbase.floatingactionbutton.FloatingActionButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class frag_home extends Fragment{


    String number = "7506931084";
    Button roster, feedback;
    FloatingActionButton soscall,sosemail;

    public frag_home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_frag_home, container, false);


        roster = view.findViewById(R.id.roster_btn);
        roster.setOnClickListener(v -> {

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://hmcs.sharepoint.com/sites/TransportRoster/Shared%20Documents/Forms/AllItems.aspx"));
            startActivity(intent);
        });


        feedback = view.findViewById(R.id.feedback_btn);
        feedback.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(view.getContext(), feedback.class);
                startActivity(intent);

            }
        });

        soscall= view.findViewById(R.id.fab_action1);
        soscall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCall();
                sendSMS();
            }
        });
        sosemail= view.findViewById(R.id.fab_action2);
        sosemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });

        return view;
    }
    private void makePhoneCall() {
        if (ContextCompat.checkSelfPermission(getView().getContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getView().getContext(), "Please grant call permissions!", Toast.LENGTH_SHORT).show();
        } else {
            String dial = "tel:" + number;
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
        }
    }

    private void sendSMS() {
        if (ContextCompat.checkSelfPermission(getView().getContext(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getView().getContext(), "Please grant sms permissions!", Toast.LENGTH_SHORT).show();
        } else {
            String msg = "Please help me ASAP, I am stuck in an emergency situation!" ;
            String dial = "tel:" + number;
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(dial, null, msg, null, null);
            Toast.makeText(getView().getContext(), "Message sent successfully!", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mail.to:"));
        String[] recipients = {"harsh.khare@here.com", "nitesh.tiwari@here.com", "nandakumar.pillai@here.com", "Transport_Helpdesk_Gigaplex@here.com",
                "Transport_Helpdesk_Airoli@here.com","ext-nilesh.satoskar@here.com","ext-jitesh.vissria@here.com"};
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Emergency! Need help!");
        intent.putExtra(Intent.EXTRA_TEXT, "Please contact me on my number, It's an emergency!  This message was sent by HereNav , @Here.com");
        intent.setType("message/rfc822");
        Intent chooser = Intent.createChooser(intent, "Send Email");
        startActivity(chooser);
    }
}





















//Below code is to navigate from one fragment to another......

      /*
            FragmentTransaction fr =   getFragmentManager().beginTransaction();
            fr.replace(R.id.fram, new roster());
            fr.commit();
      */


//below code is for calling on SOS button pressed

/*



 */
