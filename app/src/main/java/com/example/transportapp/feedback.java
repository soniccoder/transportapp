package com.example.transportapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Comment;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class feedback extends AppCompatActivity {

    public static final String Emp_id = "Employee ID ";
    public static final String Location_CN = "Location/Cab Number";
    public static final String Time = "Time ";
    public static final String Comments = "Comments";
    public static final String TAG = " Hello ";

    String Feedback[] = {"Ride Was Good", "Excellent Service", "Inappropriate Language Used By The Driver", "Driver Was Rash Driving"};
    Spinner comment;
    ArrayAdapter<String> arrayAdapter1;
    String feed;

    Calendar c = Calendar.getInstance();
    String CurrentDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

    String location;
    String Empid;
    String Feed_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        TextView Date = findViewById(R.id.date);

        EditText emp_id = findViewById(R.id.Feedback_Emp_Id);

        EditText cab_loc_num = findViewById(R.id.Feedback_Cab_Loc_Num);

        TextView time = findViewById(R.id.Feedback_Time);

//Spinner

        comment = findViewById(R.id.Feedback_Comment);

        arrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, Feedback);

        comment.setAdapter(arrayAdapter1);

//spinner end

        Date.setText(CurrentDate);

        time.setText(c.get(Calendar.HOUR) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND));

        Feed_time = time.getText().toString();


        Button Feedback_btn = findViewById(R.id.Send_Feedback);

        comment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                feed = Feedback[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Feedback_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Empid = emp_id.getText().toString();

                location = cab_loc_num.getText().toString();

                if (location.isEmpty() || Empid.isEmpty() || feed.isEmpty()) {

                    Toast.makeText(getApplicationContext(), "Kindly fill in the details..!!", Toast.LENGTH_LONG).show();
                    return;
                } else {


                    Map<String, Object> datatosave = new HashMap<String, Object>();
                    datatosave.put(Location_CN, location);
                    datatosave.put(Time, Feed_time);
                    datatosave.put(Comments, feed);
                    datatosave.put(Emp_id, Empid);

                    //  FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DocumentReference Docref = FirebaseFirestore.getInstance().collection("Feedback").document(CurrentDate).collection(Empid).document("Details");

                    Toast.makeText(getApplicationContext(), "Your Feedback Has Been Recorded", Toast.LENGTH_LONG).show();


                    Docref.set(datatosave).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                    @Override
                                                                    public void onSuccess(Void aVoid) {
                                                                        Log.d(TAG, "Document has been saved!");
                                                                    }
                                                                }
                    ).addOnFailureListener(new OnFailureListener() {
                                               @Override
                                               public void onFailure(@NonNull Exception e) {
                                                   Log.w(TAG, "Document was not saved!", e);
                                               }

                                           }

                    );
                }


            }
        });

    }
}
